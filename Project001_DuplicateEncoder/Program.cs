﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//codewars
/*    Description:
The goal of this exercise is to convert a string to a new string where each character in the new string is '(' if that character appears only once in the original string, or ')' if that character appears more than once in the original string. Ignore capitalization when determining if a character is a duplicate.

Examples:

"din" => "((("

"recede" => "()()()"

"Success" => ")())())"

"(( @" => "))(("
*/


namespace Project001_DuplicateEncoder
{
    public class Kata
    {
        public static void Main(string[] args)
        {

        }

        public static string DuplicateEncode(string word)
        {
            int licznik = 0;
            char[] tablica = new char[word.Length];
            word = word.ToLower();


            for (int j = 0; j < word.Length; j++)
            {
                for (int i = 0; i < word.Length; i++)
                {
                    if (word[j] == word[i])
                    {
                        licznik++;
                    }

                    if (licznik > 1)
                    {
                        tablica[j] = ')';

                        break;
                    }
                    else
                    {
                        tablica[j] = '(';
                    }
                }

                
                licznik = 0;

            }

            word = new string(tablica);
            


            return word;
        }
    }  

}
