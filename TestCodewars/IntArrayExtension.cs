﻿using System.Text;

namespace TestCodewars
{
    public static class IntArrayExtension
    {
        public static string IntArrayToString(this int[] array)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var item in array)
            {
                sb.Append(item);
            }

            return sb.ToString();
        }
    }
}
