﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TestCodewars
{

    [TestClass]
    public class CodewarsTests
    {
        [TestMethod]
        public void Test001DuplicateEncoder()
        {
            Assert.AreEqual(")()", Project001_DuplicateEncoder.Kata.DuplicateEncode("Ada"));
            Assert.AreEqual("(((", Project001_DuplicateEncoder.Kata.DuplicateEncode("din"));
            Assert.AreEqual("()()()", Project001_DuplicateEncoder.Kata.DuplicateEncode("recede"));
            Assert.AreEqual(")())())", Project001_DuplicateEncoder.Kata.DuplicateEncode("Success"));
            Assert.AreEqual("))((", Project001_DuplicateEncoder.Kata.DuplicateEncode("(( @"));
        }

        [TestMethod]
        public void Test002SquareEveryDigit()
        {
            Assert.AreEqual(1, Project002_SquareEveryDigit.Kata.SquareDigits(1));
            Assert.AreEqual(81, Project002_SquareEveryDigit.Kata.SquareDigits(9));
            Assert.AreEqual(811, Project002_SquareEveryDigit.Kata.SquareDigits(91));
            Assert.AreEqual(811181, Project002_SquareEveryDigit.Kata.SquareDigits(9119));
        }

        [TestMethod]
        public void TestProject003PersistentBugger()
        {
            Assert.AreEqual(0, Project003_PersistentBugger.Persist.Persistence(5));
            Assert.AreEqual(3, Project003_PersistentBugger.Persist.Persistence(39));
            Assert.AreEqual(2, Project003_PersistentBugger.Persist.Persistence(25));
            Assert.AreEqual(4, Project003_PersistentBugger.Persist.Persistence(999));
        }

        [TestMethod]
        public void TestProject004SumOfOddNumbers()
        {
            Assert.AreEqual(1, Project004_SumOfOddNumbers.Kata.rowSumOddNumbers(1));
            Assert.AreEqual(8, Project004_SumOfOddNumbers.Kata.rowSumOddNumbers(2));
            Assert.AreEqual(27, Project004_SumOfOddNumbers.Kata.rowSumOddNumbers(3));
            Assert.AreEqual(74088, Project004_SumOfOddNumbers.Kata.rowSumOddNumbers(42));
        }

        [TestMethod]
        public void TestProject005IQTest()
        {
            Assert.AreEqual(1, Project005_IQ_Test.IQ.Test("1 2 2"));
            Assert.AreEqual(3, Project005_IQ_Test.IQ.Test("2 4 7 8 10"));
        }

        [TestMethod]
        public void TestProject006UniqueInOrder()
        {
            Assert.AreEqual("", String.Join(string.Empty, Project006_UniqueInOrder.Kata.UniqueInOrder("")));
            Assert.AreEqual("ABCDAB", String.Join(string.Empty, Project006_UniqueInOrder.Kata.UniqueInOrder("AAAABBBCCDAABBB")));
            Assert.AreEqual(0, Project006_UniqueInOrder.Kata.UniqueInOrder(new int[] { 0, 1 }).ElementAt(0));
            Assert.AreEqual(1, Project006_UniqueInOrder.Kata.UniqueInOrder(new int[] { 0, 1 }).ElementAt(1));
            Assert.AreEqual(2, Project006_UniqueInOrder.Kata.UniqueInOrder(new int[] { 0, 1 }).Count());
        }

        [TestMethod]
        public void TestProject007TheSupermarketQueue()
        {
            Assert.AreEqual(0, Project007_TheSupermarketQueue.Kata.QueueTime(new int[] { }, 1));
            Assert.AreEqual(10, Project007_TheSupermarketQueue.Kata.QueueTime(new int[] { 1, 2, 3, 4 }, 1));
            Assert.AreEqual(9, Project007_TheSupermarketQueue.Kata.QueueTime(new int[] { 2, 2, 3, 3, 4, 4 }, 2));
            Assert.AreEqual(5, Project007_TheSupermarketQueue.Kata.QueueTime(new int[] { 1, 2, 3, 4, 5 }, 100));
        }

        [TestMethod]
        public void TestProject008MovingZerosToTheEnd()
        {
            Assert.AreEqual(new int[] { 1, 2, 1, 1, 3, 1, 0, 0, 0, 0 }.IntArrayToString(), Project008_MovingZerosToTheEnd.Kata.MoveZeroes(new int[] { 1, 2, 0, 1, 0, 1, 0, 3, 0, 1 }).IntArrayToString());
            Assert.AreEqual(new int[] { 1, 2, 1, 1, 3, 0, 0, 0, 0, 0 }.IntArrayToString(), Project008_MovingZerosToTheEnd.Kata.MoveZeroes(new int[] { 1, 2, 0, 1, 0, 1, 0, 3, 0, 0 }).IntArrayToString());
            Assert.AreEqual(new int[] { 1, 2, 1, 1, 3, 2, 2, 0, 0, 0 }.IntArrayToString(), Project008_MovingZerosToTheEnd.Kata.MoveZeroes(new int[] { 1, 2, 0, 1, 0, 1, 0, 3, 2, 2 }).IntArrayToString());
        }

        [TestMethod]
        public void TestProject009CreatePhoneNumber()
        {
            Assert.AreEqual("(123) 456-7890", Project009_CreatePhoneNumber.Kata.CreatePhoneNumber(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 }));
            Assert.AreEqual("(111) 111-1111", Project009_CreatePhoneNumber.Kata.CreatePhoneNumber(new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }));
        }  

        [TestMethod]
        public void test1()
        {
            Console.WriteLine("Basic Tests John");
            testJohn(11, "[0, 0, 1, 2, 2, 3, 4, 4, 5, 6, 6]");
        }

        [TestMethod]
        public void test2()
        {
            Console.WriteLine("Basic Tests Ann");
            testAnn(6, "[1, 1, 2, 2, 3, 3]");
        }
      
        [TestMethod]
        public void test3()
        {
            Console.WriteLine("Basic Tests SumAnn");
            testSumAnn(115, 4070);
        }
        [TestMethod]
        public void test4()
        {
            Console.WriteLine("Basic Tests SumJohn");
            testSumJohn(75, 1720);
        }

        private static void testSumAnn(long n, long res)
        {
            Assert.AreEqual(res, Project010_JohnAndAnnSignUpForCodewar.Johnann.SumAnn(n));
        }

        private static void testSumJohn(long n, long res)
        {
            Assert.AreEqual(res, Project010_JohnAndAnnSignUpForCodewar.Johnann.SumJohn(n));
        }

        private static string Array2String(List<long> list)
        {
            return "[" + string.Join(", ", list) + "]";
        }

        private static void testJohn(long n, string res)
        {
            Assert.AreEqual(res, Array2String(Project010_JohnAndAnnSignUpForCodewar.Johnann.John(n)));
        }

        private static void testAnn(long n, string res)
        {
            Assert.AreEqual(res, Array2String(Project010_JohnAndAnnSignUpForCodewar.Johnann.Ann(n)));
        }
    }
}
