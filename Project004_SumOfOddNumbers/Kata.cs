﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Description:
//Given the triangle of consecutive odd numbers:

//             1
//          3     5
//       7     9    11
//   13    15    17    19
//21    23    25    27    29
//...
//Calculate the row sums of this triangle from the row index (starting at index 1) e.g.:

//rowSumOddNumbers(1); // 1
//rowSumOddNumbers(2); // 3 + 5 = 8


namespace Project004_SumOfOddNumbers
{
    public static class Kata
    {
        public static long rowSumOddNumbers(long n)
        {
            var n1 = GetFirtNumberInRow(n);            
            var sum = n1;

            for (int i = 1; i < n; i++)
            {
                n1 += 2;
                sum += n1;
            }          
            
            return sum;
        }

        public static long GetFirtNumberInRow(long n)
        {
            var t = ((n * (n - 1))) + 1;

            return t;
        }



    }
}
