﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace John_and_Ann_sign_up_for_Codewars
{
    class Program
    {
        static void Main(string[] args)
        {
            
        }
    }

    public class Johnann
    {
        public static long[] JohnTab;
        public static long[] AnnTab;


        public static List<long> John(long n)
        {
            count(n);
            return new List<long>(JohnTab);
        }
        public static List<long> Ann(long n)
        {
            count(n);
            return new List<long>(AnnTab);
        }
        public static long SumJohn(long n)
        {
            long sum =0;
            count(n);
            foreach (var item in JohnTab)
            {
                sum += item;
            }

            return sum;
        }
        public static long SumAnn(long n)
        {
            long sum = 0;
            count(n);
            foreach (var item in AnnTab)
            {
                sum += item;
            }

            return sum;
        }

        public static void count(long numberOfDays)
        {
            JohnTab = new long[numberOfDays];
            AnnTab = new long[numberOfDays];
            JohnTab[0] = 0;
            AnnTab[0] = 1;

            for (int n = 1; n < numberOfDays; n++)
            {
                AnnTab[n] = n - JohnTab[AnnTab[n - 1]];
                JohnTab[n] = n - AnnTab[JohnTab[n - 1]];
            }
        }

        private static string Array2String(List<long> list)
        {
            return "[" + string.Join(", ", list) + "]";
        }
    }
}
