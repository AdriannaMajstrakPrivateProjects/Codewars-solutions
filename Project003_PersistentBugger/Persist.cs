﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Write a function, persistence, that takes in a positive parameter num
//and returns its multiplicative persistence, which is the number of times 
//you must multiply the digits in num until you reach a single digit.

namespace Project003_PersistentBugger
{
    public class Persist
    {
        public static int Persistence(long n)
        {
            string number = n.ToString();            
            long result;
            int numberOfTimes = 0;

            while (number.Length > 1)
            {
                result = 1;

                foreach (var digit in number)
                {
                    result = result * (digit - 48);
                }

                number = result.ToString();
                numberOfTimes++;
            } 
                        
            return Convert.ToInt32(numberOfTimes);
        }
    }
}
