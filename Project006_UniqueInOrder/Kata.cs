﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Description:
//Implement the function unique_in_order which takes as argument a sequence and returns a list of items without any elements with the same value next to each other and preserving the original order of elements.

namespace Project006_UniqueInOrder
{
    public static class Kata
    {
        public static IEnumerable<T> UniqueInOrder<T>(IEnumerable<T> iterable)
        {
            int i = 0;
            List<T> result = new List<T>();


            if (iterable != null && iterable.Any())
            {
                T lastElemnt = iterable.First();

                foreach (var item in iterable)
                {
                    if (i == 0)
                    {
                        i++;
                        yield return item;
                    }

                    i++;

                    if (!lastElemnt.Equals(item))
                    {
                        lastElemnt = item;
                        yield return item;
                    }
                }
            }
            else
            {
              //  yield return result;
            }
        }
    }
}
