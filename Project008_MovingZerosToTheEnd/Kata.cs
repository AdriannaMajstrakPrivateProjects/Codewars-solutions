﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project008_MovingZerosToTheEnd
{
//    Description:
//Write an algorithm that takes an array and moves all of the zeros to the end, preserving the order of the other elements.

    public class Kata
    {
        public static int[] MoveZeroes(int[] arr)
        {
            //var ordered = arr.ToList();
            //for (int i = 0; i < ordered.Count; i++)
            //{
            //    if(ordered[i] == 0)
            //    {
            //        ordered.RemoveAt(i);
            //        ordered.Insert(ordered.Count, 0);
            //    }
            //}

            int sentry=0;
            bool onlyZeros = true;

            for (int i = 0; i < arr.Length; i++)
            {
                if(arr[i] == 0)
                {
                    for (int k = i+1; k < arr.Length; k++)
                    {
                        if(arr[k]!= 0)
                        {
                            sentry = arr[k];
                            arr[k] = 0;
                            onlyZeros = false;
                            break;
                        } 
                    }

                    if(!onlyZeros)
                    {
                        arr[i] = sentry;
                        onlyZeros = true;
                    }
                    else
                    {
                        break;
                    }       
                }
            }
            return arr;
        }
    }
}
