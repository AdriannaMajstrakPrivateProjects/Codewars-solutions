﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//There is a queue for the self-checkout tills at the supermarket.Your task is write a function to calculate the total time required for all the customers to check out!

//The function has two input variables:

//customers: an array(list in python) of positive integers representing the queue.Each integer represents a customer, and its value is the amount of time they require to check out.
//n: a positive integer, the number of checkout tills.
//The function should return an integer, the total time required.

namespace Project007_TheSupermarketQueue
{
    public class Kata
    {
        public static long QueueTime(int[] customers, int n)
        {
            if (customers.Length == 0)
            {
                return 0;
            }

            IList<int> Tills = new int[n];  

            foreach (var item in customers)
            {
                Tills[Tills.IndexOf(Tills.Min())] += item;
            }
            
            return Tills.Max();
        }
    }
}
