﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project009_CreatePhoneNumber
  //  Write a function that accepts an array of 10 integers(between 0 and 9), that returns a string of those numbers in the form of a phone number.

{
    public class Kata
    {
        public static string CreatePhoneNumber(int[] numbers)
        {     
            IList<string> phoneNumberString = new List<string>();

            foreach (var item in numbers)
            {
                phoneNumberString.Add(item.ToString());
            }

            string s = phoneNumberString.ToString();

            phoneNumberString.Insert(0, "(");
            phoneNumberString.Insert(4, ")");
            phoneNumberString.Insert(5, " ");
            phoneNumberString.Insert(9, "-");

            return string.Join("", phoneNumberString.ToArray());            
        }
    }
}
